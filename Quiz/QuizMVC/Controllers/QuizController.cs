﻿using QuizMVC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace QuizMVC.Controllers
{
    public class QuizController : Controller
    {
        //private int questionCounter;
        //IDictionary<string, int> userCounter = new Dictionary<string, int>();

        // GET: Quiz
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult QuizView(string categorySelected)
        {
            if (!this.User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Login", "Login");
            }

            IEnumerable<int> questionsAnswered = null;
            using (Models.QuizEntities qe = new Models.QuizEntities())
            {

                questionsAnswered = (from quest in qe.UserAnsweredQuestions
                                     where !quest.userName.Equals(this.User.Identity.Name)
                                     select quest.question_ID).ToList();
            }

            IEnumerable<Question> q = null;

            using (Models.QuizEntities qe = new Models.QuizEntities())
            {
                q = (from question in qe.Questions
                     where question.categoryName == categorySelected && !questionsAnswered.Contains(question.question_ID)
                     select question).ToList();
            }

            //Question questionSelected = q.ElementAt(questionCounter)

            /*
            if (questionCounter == 5)
            {
                return RedirectToAction("Index", "Home");
            }
            */

            //userCounter[this.User.Identity.Name] = questionCounter;

            return View(q);
        }

        public ActionResult UserResults(FormCollection formData)
        {
            // Get User
            User userLogged = null;
            using (Models.QuizEntities qe = new Models.QuizEntities())
            {
                userLogged = (from user in qe.Users
                              where user.userName.Equals(this.User.Identity.Name)
                              select user).First();
            }


            string[] titleQuestions = formData.GetValues("title");
            int[] questionIDs = new int[titleQuestions.Length];
            int questionID;

            string[] rightOptions = new string[titleQuestions.Length];
            string rightOption;

            string currentTitle;
            for (int i = 0; i < titleQuestions.Length; i++)
            {
                currentTitle = titleQuestions[i];
                using (Models.QuizEntities qe = new Models.QuizEntities())
                {
                    
                    questionID = (from question in qe.Questions
                                     where question.title.Equals(currentTitle)
                                     select question.question_ID).First();
                }

                using (Models.QuizEntities qe = new Models.QuizEntities())
                {
                    rightOption = (from question in qe.Questions
                                   where question.question_ID == questionID
                                   select question.rightOption).First();
                }

                questionIDs[i] = questionID;
                rightOptions[i] = rightOption;
            }

            string[] optionsSelected = new string[titleQuestions.Length];

            int counter = 0;
            string name = "";
            for (int i = 0; i < optionsSelected.Length; i++)
            {
                name = counter.ToString();
                optionsSelected[i] = formData[name];
                counter++;
            }

            Models.UserAnsweredQuestion[] questionsAnswered = new Models.UserAnsweredQuestion[titleQuestions.Length];

            for (int i = 0; i < optionsSelected.Length; i++)
            {
                questionsAnswered[i] = new UserAnsweredQuestion();
                questionsAnswered[i].question_ID = questionIDs[i];
                //questionsAnswered[i].User = userLogged;
                questionsAnswered[i].userName = userLogged.userName;
                questionsAnswered[i].selectedOptionByUser = optionsSelected[i];

            }

            List<string> userInfo = new List<string>();

            userInfo.Add("Hi " + this.User.Identity.Name + "!");

            for (int i = 0; i < optionsSelected.Length; i++)
            {
                if (rightOptions[i].Equals(optionsSelected[i]))
                {
                    userInfo.Add("For question " + titleQuestions[i] + " your answer is " + optionsSelected[i] + " and it is right!");
                }
                else
                {
                    userInfo.Add("For question " + titleQuestions[i] + " your answer is " + optionsSelected[i] + " and it is wrong!");
                }
            }
            

           
            using (Models.QuizEntities qe = new Models.QuizEntities())
            {
                for (int i = 0; i < questionsAnswered.Length; i++)
                {
                    qe.UserAnsweredQuestions.Add(questionsAnswered[i]);
                }

                qe.SaveChanges();
            }

            return View(userInfo);
        }
    }
}