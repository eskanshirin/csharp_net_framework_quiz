﻿using QuizMVC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace QuizMVC.Controllers
{
    public class LoginController : Controller
    {
        public ActionResult ProcessLogin(FormCollection formData)
        {
            String username = formData["username"];
            String password = formData["password"];
            User user = null;

            using (Models.QuizEntities qe = new Models.QuizEntities())
            {
                user = (from userSelected in qe.Users
                       where userSelected.userName == username && userSelected.password == password
                       select userSelected).First();
            }

            if (user == null)
            {
                return RedirectToAction("Login", "Login");
            }

            FormsAuthentication.RedirectFromLoginPage(user.userName, false);

            return RedirectToAction("Index", "Home");

        }
        // GET: Login
        public ActionResult Login()
        {
            return View();
        }

        public ActionResult Register(FormCollection formData)
        {
            // Check if user already exist
            String username = formData["username"];
            IEnumerable<User> user = null;

            using (Models.QuizEntities qe = new Models.QuizEntities())
            {
                user = from userSelected in qe.Users
                       where userSelected.userName == username
                       select userSelected;
            }

            if (user == null)
            {
                return RedirectToAction("Login", "Login");
            }

            Models.User newUser = new Models.User();

            newUser.userName = username;
            newUser.password = formData["password"];
            newUser.firstName = formData["firstname"];
            newUser.lastName = formData["lastname"];
            newUser.email = formData["email"];

            using (Models.QuizEntities qe = new Models.QuizEntities())
            {
                qe.Users.Add(newUser);
                qe.SaveChanges();
            }

            return RedirectToAction("Index", "Home");
        }

        public ActionResult LogOff()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("Index", "Home");
        }
    }
}