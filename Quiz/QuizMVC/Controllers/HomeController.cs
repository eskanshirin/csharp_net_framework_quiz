﻿//using Quiz;
using QuizMVC.Models;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace QuizMVC.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            IEnumerable<Category> categories;
            using (Models.QuizEntities qe = new Models.QuizEntities())
            {
                categories = (from category in qe.Categories
                                  where category.Questions.Count != 0
                                  orderby category.Questions.Count
                                  select category).ToList();
            }

            return View(categories);
        }

        public ActionResult AddCategory()
        {
            return View();
        }

        public ActionResult AddQuestion()
        {
            IEnumerable<Category> categories;
            using (Models.QuizEntities qe = new Models.QuizEntities())
            {
                categories = (from category in qe.Categories
                              where category.Questions.Count != 0
                              orderby category.Questions.Count()
                              select category).ToList();
            }

            return View(categories);
        }

        public ActionResult AddCategoryToDataBase(FormCollection formData)
        {
            Category newCat = new Category();
            newCat.categoryName = formData["newCat"];
            //validate that category doesnt exist
            using (Models.QuizEntities qe = new Models.QuizEntities())
            {
               
                qe.Categories.Add(newCat);
                qe.SaveChanges();
            }
            return RedirectToAction("Index", "Home");

        }
        public ActionResult AddQuestionToDataBase(FormCollection formData)
        {
            string formCategory = formData["category"];
            string formUserName = formData["userName"];


            IEnumerable <Category> categoryCollection = null;

            using (Models.QuizEntities qe = new Models.QuizEntities())
            {
                categoryCollection = (from category in qe.Categories
                                     where category.categoryName == formCategory
                                     select category).ToList();
            }

            Category categoryChosen = categoryCollection.First();

            IEnumerable<User> userCollection = null;

            using (Models.QuizEntities qe = new Models.QuizEntities())
            {
                userCollection = (from user in qe.Users
                                  where user.userName == formUserName
                                  select user).ToList();
            }

            User userAuthor = userCollection.First();

            Question newQuestion = new Question();
            newQuestion.title = formData["newQuestionTitle"];
            newQuestion.categoryName = formData["category"];
            newQuestion.option1 = formData["option1"];
            newQuestion.option2 = formData["option2"];
            newQuestion.option3 = formData["option3"];
            newQuestion.option4 = formData["option4"];
            newQuestion.rightOption = formData["rightOption"];
            newQuestion.userNameAuthor = formData["userName"];
            newQuestion.Category = categoryChosen;
            newQuestion.User = userAuthor;

            using (Models.QuizEntities qe = new Models.QuizEntities())
            {

                qe.Questions.Add(newQuestion);
                qe.SaveChanges();
            }
            return RedirectToAction("Index", "Home");

        }
    }
 }
