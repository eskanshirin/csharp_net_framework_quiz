﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Quiz
{
    public class Program
    {
        public static XElement RetrieveOptions(XElement question)
        {
            var correctOption = from o in question.Elements("option")
                                where o.Attribute("correct") != null
                                select o;

            return correctOption.First();
        }

        public static XElement ReturnWrongOption(XElement question, string num)
        {
            var option = from o in question.Elements("option")
                         where o.Attribute("num").Value.Equals(num)
                         select o;

            return option.First();
        }

        public static User GetUser (IEnumerable<User> users, string userName)
        {
            foreach (var user in users)
            {
                if (user.userName.Equals(userName))
                {
                    return user;
                }
            }

            return null;
        }

        public static Category GetCategory(IEnumerable<Category> categories, string categoryName)
        {
            foreach (var category in categories)
            {
                if (category.categoryName.Equals(categoryName))
                {
                    return category;
                }
            }

            return null;
        }

        public static string AssignPasswordOrEmail(bool isEmail, ref int counter, string[] emails, string[] passwords)
        {
            string returnValue;

            if (isEmail)
            {
                returnValue = emails[counter];
            }
            else
            {
                returnValue = passwords[counter];
            }

            counter++;
            return returnValue;
        }

        public static void Main(string[] args)
        {
            int counterEmail = 0;
            int counterPass = 0;
            String[] emailsData =
            {
                "benSmyth2009@gmail.com",
                "lamountThecool20@gmai.com",
                "MooseMOOO@yahoo.com",
                "LightRoom@gmail.ir"
            };

            String[] passwordsData =
            {
                "benSmythPassword",
                "lamountPaaword",
                "MooseMOOOPaaword",
                "Light"
            };

            XElement usersXML = XDocument.Load(@"users.xml").Root;

            //select the users from xml
            IEnumerable<User> usersCollection = (from u in usersXML.Elements("user")
                       select new User
                       {
                           lastName = ReturnValueOrNull(u,"lastName"),
                           firstName = ReturnValueOrNull(u, "firstName"),
                           userName = ReturnValueOrNull(u, "userId"),
                           email =  AssignPasswordOrEmail(true, ref counterEmail, emailsData, passwordsData),
                           password = AssignPasswordOrEmail(false, ref counterPass, emailsData, passwordsData)
                       }).ToList();


            foreach (var user in usersCollection)
            {
                Console.WriteLine("email: " + user.email + " password: " + user.password);
            }

            
            
            using (QuizEntities qe = new QuizEntities())
            {
                foreach (User u in usersCollection)
                {
         //           qe.Users.Add(u);
                }
                qe.SaveChanges();

            }//end using

            XElement questionsXML = XDocument.Load(@"questions.xml").Root;
            IEnumerable<String> categoryNames = (from c in questionsXML.Descendants("question").Elements("category")
                                              select c.Value.Trim()).Distinct();
            IEnumerable<Category> categories = (from c in categoryNames
                                               select new Category
                                               {
                                                   categoryName = c
                                               }).ToList();

            HelperDeleteCategory(categories);
            using (QuizEntities qe = new QuizEntities())
            {
                foreach (Category cat in categories)
                {
            //        qe.Categories.Add(cat);
                }

                qe.SaveChanges();
            }

            IEnumerable<Question> questions = (from q in questionsXML.Elements("question")
                                               select new Question
                                               {
                                                   categoryName = q.Element("category").Value.Trim(),
                                                   title = q.Element("title").Value.Trim(),
                                                   option1 = ReturnWrongOption(q, "1").Value.Trim(),
                                                   option2 = ReturnWrongOption(q, "2").Value.Trim(),
                                                   option3 = ReturnWrongOption(q, "3").Value.Trim(),
                                                   option4 = ReturnWrongOption(q, "4").Value.Trim(),
                                                   rightOption = RetrieveOptions(q).Value.Trim(),
                                                   userNameAuthor = q.Element("user").Value.Trim(),
                                                   User = GetUser(usersCollection, q.Element("user").Value.Trim()),
                                                   Category = GetCategory(categories, q.Element("category").Value.Trim())
                                               }).ToList();
            
            HelperDeleteQuestions(questions);
            HelperDelete(usersCollection);

            using (QuizEntities qe = new QuizEntities())
            {
                foreach (Question q in questions)
                {
                    qe.Questions.Add(q);
                }

                qe.SaveChanges();
            }
       
        }// End Main method

        public static String ReturnValueOrNull(XElement xmlElement, string attributeName)
        {
            if (xmlElement.Attribute(attributeName) == null)
            {
                return "notSetYet";
            }
            else
            {
                return xmlElement.Attribute(attributeName).Value;
            }
        }//end method 

        private static void HelperDelete(IEnumerable<User> users)
        {
            using (QuizEntities qe = new QuizEntities()) //database
            {
                qe.Users.RemoveRange(qe.Users);
                qe.SaveChanges();

            }//end using
        }

        private static void HelperDeleteCategory(IEnumerable<Category> categories)
        {
            using (QuizEntities qe = new QuizEntities()) //database
            {
                qe.Categories.RemoveRange(qe.Categories);
                qe.SaveChanges();

            }//end using
        }

            private static void HelperDeleteQuestions(IEnumerable<Question> question)
            {
                using (QuizEntities qe = new QuizEntities()) //database
                {
                    qe.Questions.RemoveRange(qe.Questions);
                    qe.SaveChanges();

                }//end using
            }
    }
}
